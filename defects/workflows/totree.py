"""
From an ase.db of host structures, generate defects and write into a directory
structure which can be equipped with ASR-defect workflows.

To use the workflow first copy the example workflow to your cwd
cp  ... / asr-defects/Examples/totree_example_wf.py .

Then change the input dict and database name in the example
according to your needs

Then initialize a repo and run the workflow
tb init defects
tb workflow totree_example_wf.py

In order to utilize the materials created by this file the main
workflow.py file under the def workflow function must have
@tb.parametrize_glob('*/material')
added above it and the input must accept material as a variable.

To change default arguments use the function generate_input_dict:

Defect setup details:

    Set up all possible defects within a reasonable supercell
    as well as the respective pristine system for a given input structure.
    Defects include: vacancies,
    intrinsic substitutional defects. For a given primitive input structure
    this recipe will create a directory tree in the following way
    (for the example of BN):

    - There has to be a 'unrelaxed.json' file with the primitive structure
      of the desired system in the folder you run setup.defects. The tree
      structure will then look like this:
tree
└── MoS2
    ├── defects.MoS2_331.Mo_S
    │   └── material
    │       ├── input.json
    │       └── output.json
    ├── defects.MoS2_331.S_Mo
    │   └── material
    │       ├── input.json
    │       └── output.json
    ├── defects.MoS2_331.v_Mo
    │   └── material
    │       ├── input.json
    │       └── output.json
    ├── defects.MoS2_331.v_S
    │   └── material
    │       ├── input.json
    │       └── output.json
    └── defects.pristine_sc.331
        └── material
            ├── input.json
            └── output.json

    - Here, the notation for the defects is the following:
      'formula_supercellsize.defect_substitutionposition'
      where 'v' denotes a vacancy
    - When the general algorithm is used to set up symmetry broken supercells,
      the foldernames will contain '000' instead of the supersize.
    - In the resulting folders you can find the unrelaxed structures.
    """


import sys
import numpy as np
from ase.db import connect


def defecttotree(definitions, name, root=None):

    def workflow(rn):
        if root is not None:
            print(root)
            rn = rn.with_subdirectory(root)

        for host_key, defect_dict in definitions.items():
            for defect_key, defect in defect_dict.items():
                rn1 = rn.with_subdirectory(f'{host_key}/{defect_key}')
                taskdir = f'{host_key}/{defect_key}/{name}'
                rn1.define(defect, taskdir)

    return workflow


def add_materials_from_db(filename: str) -> dict:
    """
    Helper function for a high-throughput workflow that loads an ase.db, reads
    the structures, and adds them to the tb workflow.

    :param filename: the file path + filename of the ase database.
    :return: a dictionary containing chemical formula: ase atoms object for
    each structure.
    """
    cforms = []
    db_structures = {}
    # connect local db file and
    with connect(filename) as con:
        for row in con.select():
            atoms = row.toatoms().copy()  # extra copy to remove calculators
            cform = atoms.get_chemical_formula()
            cforms.append(cform)
            db_structures[cform] = atoms

    # tell the user to change the naming convention for the dictionary if 2
    # structs have same chem formula
    if len(cforms) != len(np.unique(cforms)):
        print()
        sys.exit('The naming convention for the dictionary has '
                 'duplicates and a structure was overwritten. '
                 'Change the naming convention to prevent this.')

    return db_structures
