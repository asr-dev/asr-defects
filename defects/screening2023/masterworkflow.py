import taskblaster as tb
from defects.workflows.basic_workflow import BasicDefectWorkflow
from defects.workflows.screening import LowParamsScreening

"""
20230622: Added Relaxations
20230612: Added gs
"""


@tb.workflow
class ScreeningWorkflow:
    """
    Master workflow for the new screening project.
    More subworkflows will be added as the project progresses,
    but let's begin with relaxations.
    """

    initial_atoms = tb.var()
    calc_lowparams = tb.var()
    calc_relax = tb.var()
    calc_gs = tb.var()
    oqmd = tb.var()
    c2db = tb.var()
    host_folder = tb.var()
    max_eform = tb.var()

    @tb.subworkflow
    def LowParams(self):
        return LowParamsScreening(
            initial_atoms=self.initial_atoms,
            calc=self.calc_lowparams,
            oqmd=self.oqmd,
            c2db=self.c2db,
            host_folder=self.host_folder,
            max_eform=self.max_eform)

    # Relax all defects in 0, +/- 1 charge states
    @tb.subworkflow
    def BasicWF(self):
        return BasicDefectWorkflow(
            initial_atoms=self.LowParams.physical_defect['atoms'],
            calc_relax=self.calc_relax,
            calc_gs=self.calc_gs)

    # Add ZFS, HF coupling and Symmetry analysis for all defects.
    # Screen for doublets and triplets before excited state calculations
