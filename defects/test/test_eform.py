import numpy as np
import os
from ase.io import read


def test_hof_and_db():
    from defects.tasks.eform import get_heat_of_formation
    from defects.database.material_fingerprint import get_uid_of_atoms
    from defects.database.material_fingerprint import get_hash_of_atoms

    db = '/home/niflheim2/cmr/WIP/doubledefects/databases/c2db.db'
    # has to be identical to c2db structure
    atoms = read('hosts/structure_BN.json')
    hash = get_hash_of_atoms(atoms)
    assert hash == '4a5edc7636042e53b27782b72acbbaf6'
    uid = get_uid_of_atoms(atoms, hash)
    assert uid == 'BN-4a5edc763604'
    hof = get_heat_of_formation(db, atoms)
    assert np.isclose(hof, -1.2477745531868054)


def test_eform():
    from defects.tasks.eform import get_all_eforms
    from ase.io import read
    from pathlib import Path
    from ase.calculators.calculator import compare_atoms
    import shutil

    # has to be identical to c2db structure
    atoms = read('hosts/structure_BN.json')
    cwd = Path('.').absolute()
    hostdir = Path(str(cwd)+'/hosts/')

    # make realistic folder structure to test search functions
    # Clean up
    try:
        shutil.rmtree(str(cwd)+'/BN')
    except FileNotFoundError:
        print("Nothing to clean up...")
    os.mkdir('BN')
    os.chdir('BN')
    os.mkdir('defects.BN_000.v_B')
    os.chdir('defects.BN_000.v_B')
    os.mkdir('whatever')
    os.chdir('whatever')
    os.mkdir('tmp')
    os.chdir('tmp')

    etot_def = 25.
    etot_pris = 10.
    oqmd = '/home/niflheim/fafb/db/oqmd12.db'
    c2db = '/home/niflheim2/cmr/WIP/doubledefects/databases/c2db.db'

    output = get_all_eforms(atoms, oqmd, c2db, hostdir, etot_def, etot_pris)

    atoms_new = output['atoms']
    # Check so that nothing was done to the atoms
    assert compare_atoms(atoms, atoms_new) == []
    eform_test = {'standard-states': 8.295315858321331,
                  'B-poor': 7.0475413051345255,
                  'N-poor': 8.295315858321331,
                  'min_eform': 7.0475413051345255}

    eform = output['eform']
    is_defect = output['is_defect']

    assert is_defect
    for key, item in eform_test.items():
        assert np.isclose(eform[key], item)
    # Clean up
    shutil.rmtree(str(cwd)+'/BN')
