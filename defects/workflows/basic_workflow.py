import taskblaster as tb


@tb.workflow
class BasicWorkflow:
    initial_atoms = tb.var()
    calc_relax = tb.var()
    calc_gs = tb.var()
    charge = tb.var()

    @tb.task
    def relax(self):
        return tb.node('defects.tasks.relax.relax_fixcell',
                       atoms=self.initial_atoms,
                       calculator=self.calc_relax,
                       charge=self.charge)

    @tb.task
    def gs(self):
        return tb.node('defects.tasks.gs.defect_gs',
                       atoms=self.relax['atoms'],
                       calculator=self.calc_gs,
                       charge=self.charge)

    @tb.task
    def zfs(self):
        return tb.node('defects.tasks.zfs.zero_field_splitting',
                       calc_path=self.gs['gpw_path'],
                       magmom=self.gs['magmom'])


@tb.workflow
class BasicDefectWorkflow:
    """
    Workflow for performing structural relaxation and gs
    calculation of defects in 0, +/- 1 charge states
    """
    initial_atoms = tb.var()
    calc_relax = tb.var()
    calc_gs = tb.var()

    @tb.subworkflow
    def charge_0(self):
        return BasicWorkflow(
            initial_atoms=self.initial_atoms,
            calc_relax=self.calc_relax,
            calc_gs=self.calc_gs,
            charge=0)

    @tb.subworkflow
    def charge_1(self):
        return BasicWorkflow(
            initial_atoms=self.charge_0.relax['atoms'],
            calc_relax=self.calc_relax,
            calc_gs=self.calc_gs,
            charge=1)

    @tb.subworkflow
    def charge_m1(self):
        return BasicWorkflow(
            initial_atoms=self.charge_0.relax['atoms'],
            calc_relax=self.calc_relax,
            calc_gs=self.calc_gs,
            charge=-1)
