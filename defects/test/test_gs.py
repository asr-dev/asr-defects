from defects.test.materials import std_test_materials_dict


def test_defect_gs(repo):
    """
    Simple test of gs task
    """
    from defects.tasks.gs import defect_gs
    from gpaw import GPAW
    from pathlib import Path

    atoms = std_test_materials_dict['BN']
    calculator = {'mode': {'name': 'pw', 'ecut': 200}, 'xc': 'PBE',
                  'kpts': {'density': 1},
                  'txt': 'gs.txt'
                  }

    output = defect_gs(atoms, calculator)
    # check magnetic moment
    assert abs(output['magmom']) < 0.0001
    # check gpw file
    _ = GPAW(output['gpw_path'])
    # check path and output file
    assert Path(output['path']/'gs.txt').is_file()
