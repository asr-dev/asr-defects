from taskblaster.repository import Repository
from taskblaster import JSONCodec


class ASECodec(JSONCodec):
    def encode(self, obj):
        from ase.io.jsonio import default
        return default(obj)

    def decode(self, dct):
        from ase.io.jsonio import object_hook
        return object_hook(dct)


class DefectsRepository(Repository):
    def mpi_world(self):
        from defects.mpiworker import TBCommunicator
        try:
            from gpaw.mpi import world
        except ModuleNotFoundError:
            return super().mpi_world()

        return TBCommunicator(world)


# Magical "initializer" for taskblaster repository using existing
# asr encoder.  Taskblaster imports this and uses it to initialize
# any repository.
def tb_init_repo(root):
    return DefectsRepository(
        root, usercodec=ASECodec())
