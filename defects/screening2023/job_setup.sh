#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=fanni@dtu.dk  # The default value is the submitting user.
#SBATCH --partition=xeon40
#SBATCH -N 1      # Minimum of 2 nodes
#SBATCH -n 40     # 24 MPI processes per node, 48 tasks in total, appropriate for xeon24 nodes
#SBATCH --time=0-01:00:00
#SBATCH --output=setup_new.log
#SBATCH --error=setup_errors_new.log

source ../../venv/bin/activate

tb workflow generate_defects.py
