from pathlib import Path
from ase.db import connect
from ase.io import read


def physical_defect(eforms_dict, max_eform):
    """
    eforms_dict = get_all_eforms(atoms, oqmd, c2db, host_path,
                                 etot_def, etot_pris)
    """
    if eforms_dict['is_defect']:
        min_eform = eforms_dict['eform']['min_eform']
        if min_eform > max_eform:
            raise ValueError("Formation energy  too large. \
            Discarding unphysical defect...")
    return eforms_dict


def get_all_eforms(atoms, oqmd, c2db, host_path, etot_def, etot_pris):
    defectinfo = DefectInfo()
    eform, sstates = calculate_neutral_formation_energy(etot_def,
                                                        etot_pris,
                                                        oqmd,
                                                        defectinfo)
    if 'pristine' in defectinfo.names:
        return {'atoms': atoms, 'eform': 0, 'is_defect': False}

    host_atoms = find_host_structure(host_path)
    hof = get_heat_of_formation(c2db, host_atoms)
    el_list = get_element_list(host_atoms)
    eform_dict = {}
    eform_dict['standard-states'] = eform
    min_eform = eform
    # EF = 0 valid for uncharged defects, update if consider charge states
    for element in el_list:
        mu = get_adjusted_chemical_potentials(host_atoms, hof, element)
        # Adjust to element-poor conditions
        new_eform = adjust_formation_energies(defectinfo, eform, mu)
        eform_dict[element+'-poor'] = new_eform
        if new_eform < min_eform:
            min_eform = new_eform
    eform_dict['min_eform'] = min_eform
    output = {'atoms': atoms, 'eform': eform_dict, 'is_defect': True}
    return output


def get_element_list(atoms):
    """Return list of unique chem. elements of a structure."""
    symbollist = []
    for i, atom in enumerate(atoms):
        symbol = atoms.symbols[i]
        if symbol not in symbollist:
            symbollist.append(symbol)

    return symbollist


def get_stoichiometry(atoms, reduced=False):
    from ase.formula import Formula

    if reduced:
        w = Formula(atoms.get_chemical_formula()).stoichiometry()[1]
    else:
        w = Formula(atoms.get_chemical_formula())

    return w.count()


def get_adjusted_chemical_potentials(host, hof, element):
    el_list = get_element_list(host)
    stoi = get_stoichiometry(host)
    sstates = {}
    for el in el_list:
        name = el
        if el == element:
            mu_el = hof / stoi[element]
            sstates[f'{name}'] = mu_el
        else:
            sstates[f'{name}'] = 0

    return sstates


def adjust_formation_energies(defectinfo, eform,  mu):
    """Return defect dict extrapolated to mu chemical potential conditions."""
    # sstates = get_adjusted_chemical_potentials(host, hof, element)
    defects = defectinfo.names
    for defectname in defects:
        def_type, def_pos = \
                defectinfo.get_defect_type_and_kind_from_defectname(
                    defectname)

        # If mu for substitutional defect has not been added set to zero
        if f'{def_type}' not in mu:
            mu[f'{def_type}'] = 0.0
        if def_type == 'v':
            add = 0
            remove = mu[f'{def_pos}']
        elif def_pos == 'i':
            add = mu[f'{def_type}']
            remove = 0
        else:
            add = mu[f'{def_type}']
            remove = mu[f'{def_pos}']
        # adjust formation energy for defects one by one
        eform = eform - add + remove
    return eform


def find_host_structure(host_path):
    """
    Assumes there is a file called *hostname*.json
    in host_path
    """
    defectpath = find_defect_path()
    host = defectpath.parent
    hostname = str(host)
    hostname = hostname.split('/')[-1]
    hostpaths = list(Path(host_path).glob('*'+hostname+'*.json'))
    assert len(hostpaths) == 1
    for path in hostpaths:
        host = read(path)
    return host


def get_energies_and_structures(atoms, subdir, filename):
    pristine_structure_path = find_pristine_structure_path(subdir, filename)
    defect_path = find_defect_path()
    defect_structure_path = Path(defect_path, Path(subdir), Path(filename))
    atoms_def = read(defect_structure_path)  # To get atoms with calculator
    etot_def = atoms_def.get_potential_energy()
    atoms_pris = read(pristine_structure_path)
    etot_pris = atoms_pris.get_potential_energy()
    output = {'atoms': atoms, 'atoms_pris': atoms_pris,
              'etot_def': etot_def, 'etot_pris': etot_pris}
    return output


def find_pristine_structure_path(subdir, filename):
    defectpath = find_defect_path()
    host = defectpath.parent
    prispath = list(host.glob('*pristine*'))[0]
    pris_structure_path = Path(prispath,
                               Path(subdir),
                               Path(filename))
    assert pris_structure_path.is_file()
    return pris_structure_path


def find_defect_path():
    calcpath = Path('.').absolute()
    defectpath = None
    for ppath in calcpath.parents:
        if 'defect' in ppath.name:
            defectpath = Path(ppath)
            break
    assert defectpath is not None
    return defectpath


def obtain_chemical_potential(symbol, db):
    """Extract the standard state of a given element."""
    energies_ss = []
    db = connect(db)
    if symbol == 'v' or symbol == 'i':
        eref = 0.
    else:
        for row in db.select(symbol, ns=1):
            energies_ss.append(row.energy / row.natoms)
        eref = min(energies_ss)

    return {'element': symbol, 'eref': eref}


def calculate_neutral_formation_energy(etot_def, etot_pris, db, defectinfo):
    """Calculate the neutral formation energy with chemical potential shift.

    Only the neutral one is needed as for the higher charge states we
    will use the sj transitions for the formation energy plot.

    Formation energy is calculated w r t standard states
    """
    eform = etot_def - etot_pris
    if 'pristine' in defectinfo.names:
        return 0.0, None
    # next, extract standard state energies for particular defect
    for defect in defectinfo.names:
        def_add, def_remove = \
           defectinfo.get_defect_type_and_kind_from_defectname(defect)
        # extract standard states of defect atoms from OQMD
        standard_states = []
        standard_states.append(obtain_chemical_potential(def_add, db))
        standard_states.append(obtain_chemical_potential(def_remove, db))
        # add, subtract chemical potentials to/from formation energy
        eform = eform - standard_states[0]['eref'] + standard_states[1]['eref']

    return eform, standard_states


def get_heat_of_formation(db, atoms):
    """Extract heat of formation from C2DB."""
    from defects.database.material_fingerprint import get_uid_of_atoms
    from defects.database.material_fingerprint import get_hash_of_atoms

    hash = get_hash_of_atoms(atoms)
    uid = get_uid_of_atoms(atoms, hash)
    db = connect(db)

    for row in db.select(uid=uid):
        hof = row.hform

    return hof


class DefectInfo:
    """Class containing all information about a specific defect."""

    def __init__(self,
                 defecttoken=None):
        if defecttoken is None:
            defecttoken = self._defect_token_from_path()
        self.names, self.specs = self._defects_from_token(
            defecttoken=defecttoken)

    def _defect_token_from_path(self):
        # Getting info from cwd
        defectpath = find_defect_path()
        dirname = defectpath.name
        if 'pristine' in dirname:
            defecttoken = ['pristine']
        else:
            defecttoken = dirname.split('.')[2:]
        return defecttoken

    def _defects_from_token(self, defecttoken=None):
        """Return defecttype, and kind."""
        if len(defecttoken) >= 2:
            defects = defecttoken[:-1]
            specs_str = defecttoken[-1].split('-')
            specs = [int(spec) for spec in specs_str]
        else:
            defects = defecttoken
            specs = [0]

        return defects, specs

    def get_defect_type_and_kind_from_defectname(self, defectname):
        tokens = defectname.split('_')
        return tokens[0], tokens[1]

    def is_vacancy(self, defectname):
        return defectname.split('_')[0] == 'v'

    def is_interstitial(self, defectname):
        return defectname.split('_')[0] == 'i'

    @property
    def number_of_vacancies(self):
        Nvac = 0
        for name in self.names:
            if self.is_vacancy(name):
                Nvac += 1

        return Nvac
