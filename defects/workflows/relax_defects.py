import taskblaster as tb


@tb.workflow
class RelaxWorkflow:
    initial_atoms = tb.var()
    calc = tb.var()
    charge = tb.var()

    @tb.task
    def relax(self):
        return tb.node('defects.tasks.relax.relax_fixcell',
                       atoms=self.initial_atoms,
                       calculator=self.calc,
                       charge=self.charge)


@tb.workflow
class RelaxDefectWorkflow:
    """
    Workflow for performing structural relaxation of defects
    in 0, +/- 1 charge states
    """
    initial_atoms = tb.var()
    calc = tb.var()

    @tb.subworkflow
    def charge_0(self):
        return RelaxWorkflow(
            initial_atoms=self.initial_atoms,
            calc=self.calc,
            charge=0)

    @tb.subworkflow
    def charge_1(self):
        return RelaxWorkflow(
            initial_atoms=self.charge_0.relax['atoms'],
            calc=self.calc,
            charge=1)

    @tb.subworkflow
    def charge_m1(self):
        return RelaxWorkflow(
            initial_atoms=self.charge_0.relax['atoms'],
            calc=self.calc,
            charge=-1)
