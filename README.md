# Welcome to asr-defects
The ASR DEFECTS workflow repository is a 
high-throughput workflow project for predicting the properties of defects in
2D materials.

Check out the doc pages [here](https://asr-dev.gitlab.io/asr-defects/).

# Running the workflow
```
$ tb init defects
$ cp ../asr-defects/Examples/totree_example_wf.py .
$ cp ../asr-defects/Examples/relax_example_wf.py .
$ tb workflow totree_example_wf.py
$ tb workflow relax_example_wf.py
$ tb submit tree
$ tb submit-worker -R 40:xeon40:24h -j <number of workers>

```

# Instructions for developers

All development should be done in seperate branches, that branch off
from master

```
git checkout master
git checkout -b <branch_name>
```

Commit changes regularly

```
git add <file_name>
git commit -m "descriptive message"
```

When you feel ready push to remote in new branch
```
git push --set-upstream origin <branch_name>
```

.. Make further changes...

Check flakes

```
flake8 <filename>
```

Finally make a merge request to master. (Information on how to create MR 
is printed when you push the branch)

# SCREENING PROJECT 2023

```
cd /home/niflheim2/cmr/WIP/defect-screening-2023
source venv/bin/activate
cd calculations
mkdir <your_name>
cd <your_name>
mkdir hosts
```
Download host structure from c2db and save as hosts/structure_<host>.json

Copy scripts from repo

```
cp ../../venv/asr-defects/defects/screening2023/generate_defects.py .
cp ../../venv/asr-defects/defects/screening2023/main_wf.py .
cp ../../venv/asr-defects/defects/screening2023/job_setup.sh .
```

1. Change the host name in generate_defects.py
2. Change the host structure path ion main_wf.py
Initialize repo and generate defects
Note: setting up defects for complex structures
is done using tb workflow. However, this turned out to
be too heavy to do directly on login nodes, therefore we
submit using the batch-script for now. XXX This should be changed!

```
tb init defects
sbatch job_setup.sh
```
`Wait until setup job has finished and then check so that
the generated defects look reasonable
```
$ tree tree
```

and run dummy task of defect generation
```
tb run tree
```

then submit main workflow
```
tb workflow main_wf.py
```

Start by submitting low-params relaxations
```
tb submit tree/<host>/defects*/LowParams/relax_lowparams
tb submit-worker -R 80:xeon40:48h -j <number of workers> --max-tasks 1
```
You should submit as many workers as you have queued jobs. The number of jobs in the queue can be checked by
```
tb stat
```

NOTE! Due to the way the workflow is written you have to make sure that the 
pristine relaxation is finished before continuing to submit jobs.

After that you can collect the results, run the formation energy tasks and
screening for low formation energy (physical_defect)

```
tb run tree/<host>/defects*/LowParams/collect_results
tb run tree/<host>/defects*/LowParams/formation energies
tb run tree/BN/defects*/LowParams/physical_defect
```

You are now ready to submit the full params relaxations
```
tb submit tree/<host>/*/BasicWF/charge_0/relax
tb submit-worker -R 40:xeon40:48h -j <number of workers> --max-tasks 1
```
When jobs finish check so that everything seems alright and submit charge calculations in the same way. After that you can continue to submit gs and zfs.

If relax jobs time-out they will probably still be marked as running by tb. To check if they timed out
use myqueue or slurm to see if you have jobs in the queue.

Timed out relaxations can be restarted from the last trajectory file like this
```
gpaw python ../../scripts/hacks/unrun.py
tb submit tree/<host>/*/BasicWF/charge_0/relax
tb submit-worker -R 80:xeon40:48h -j <number of workers> --max-tasks 1
```
where we have doubled the cores to make sure that the jobs have sufficient resources.

The rest of the relaxations and gs calculations can then be submitted in a similar way.

Good Luck :)!