from pathlib import Path
from ase import Atoms


def defect_gs(atoms: Atoms, calculator:  dict,
              comm=None, charge=0) -> dict:
    """
    Task to perform an electronic groundstate calculation

    :param atoms: ASE atoms object coming from the structure relaxation
    :param calculator: A dictionary containing the calculator input parameters
    :param comm: The parallel communicator used to properly write files
    :param charge: float. Change the charge in the calculator
    :return: Path object for the ground state file gs.gpw
    """
    from asrlib.tasks.factory import GPAWFactory
    from asrlib.tasks.mpiprocesses import gpaw

    if comm is None:
        from ase.parallel import world
        comm = world

    calculator['charge'] = charge
    factory = GPAWFactory(calculator=calculator)
    factory.setup_kpts(atoms)

    with gpaw(factory, comm) as atoms.calc:
        atoms.get_potential_energy()
        path = Path('gs.gpw')
        atoms.calc.write(path)
        magmom = atoms.calc.get_magnetic_moment()
    matpath = Path('.')
    return {'gpw_path': path, 'magmom': magmom, 'path': matpath}
