import numpy as np
from pathlib import Path


def zero_field_splitting(calc_path: Path, magmom: float):
    """Calculate zero-field-splitting."""
    from gpaw import restart

    # read in atoms and calculator, check whether spin and magnetic
    # moment are suitable, i.e. spin-polarized triplet systems
    atoms, calc = restart(calc_path, txt=None)
    is_triplet = check_magmoms(magmom)

    if not is_triplet:
        return {'is_triplet': is_triplet, 'D_vv': np.zeros([3, 3])}

    # run fixed density calculation
    calc = calc.fixed_density(kpts={'size': (1, 1, 1), 'gamma': True})

    # evaluate zero field splitting components for both spin channels
    D_vv = get_zfs_components(calc)

    return {'is_triplet': is_triplet, 'D_vv': D_vv}


def get_zfs_components(calc):
    """Get ZFS components from GPAW function, and convert tensor scale."""
    from ase.units import _e, _hplanck
    from gpaw.zero_field_splitting import convert_tensor, zfs

    D_vv = zfs(calc)
    scale = _e / _hplanck * 1e-6
    D, E, axis, D_vv = convert_tensor(D_vv * scale)

    return D_vv


def check_magmoms(magmom, target_magmom=2, threshold=1e-1):
    """Check whether input atoms are a triplet system."""
    if not abs(magmom - target_magmom) < threshold:
        return False
    else:
        return True
