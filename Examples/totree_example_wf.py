from defects.workflows.totree import defecttotree, add_materials_from_db
from defects.tasks.setup_defects import setup_defect_dict

# Here you can add kwargs in a dictionary for defect generation.
# See see asr-defects.defects.tasks.setup_defects.setup_defect_dict
kwargs = {}


# Helper function to unbpack ase db.
# Alternatively user can provide a dict of structures directly
filename = 'materials.db'
host_dict = add_materials_from_db(filename)

# totree workflow
workflow = defecttotree(setup_defect_dict(host_dict, **kwargs),
                        name='material')
