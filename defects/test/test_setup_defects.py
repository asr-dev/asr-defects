import pytest
from defects.test.materials import std_test_materials, GaAs
from defects.test.materials import std_test_materials_dict
from defects.tasks.setup_defects import setup_defect_dict


def test_nearest_distance():
    from defects.tasks.setup_defects import return_distances_cell
    import numpy as np

    atoms = std_test_materials[1]
    cell = atoms.get_cell()
    distances = return_distances_cell(cell)
    refs = [cell[0][0], np.sqrt(cell[1][0]**2 + cell[1][1]**2),
            np.sqrt((-cell[0][0] + cell[1][0])**2 + cell[1][1]**2),
            np.sqrt((cell[0][0] + cell[1][0])**2 + cell[1][1]**2)]
    for i, dist in enumerate(distances):
        assert dist == refs[i]


def test_setup_defects(repo):
    from pathlib import Path
    # from ase.calculators.calculator import compare_atoms
    # import json
    from defects.workflows.totree import defecttotree
    pathname = 'material*'
    atoms = std_test_materials[1]
    host_dict = {}
    host_dict['BN'] = atoms
    kwargs = {}
    kwargs['supercell'] = (3, 3, 1)
    rn = repo.runner()
    workflow = defecttotree(setup_defect_dict(host_dict, **kwargs),
                            name='material')
    workflow(rn)
    atoms = atoms.repeat((3, 3, 1))

    # skip for now...
    """
    # Opening input.json file
    codec = ASECodec #ASRJSONCodec()
    with open('tree/BN/defects.pristine_sc.331/material/input.json') as f:
        pristine = json.load(f, object_hook=codec.decode)[1]['obj']
    print(pristine)
    assert compare_atoms(atoms, pristine) == []
    """
    pathlist = list(Path('.').glob('tree/BN/defects.pristine_sc.331/' +
                                   pathname))
    assert len(pathlist) == 1
    assert Path(pathlist[0] / 'input.json').is_file()

    pathlist = list(Path('.').glob('tree/BN/defects.BN_331*/' + pathname))
    for path in pathlist:
        assert Path(path / 'input.json').is_file()

    pathlist = list(Path('.').glob('tree/BN/defects.BN_331*'))
    pathlist = [str(thispath) for thispath in pathlist]
    complist = ['tree/BN/defects.BN_331.v_B',
                'tree/BN/defects.BN_331.v_N',
                'tree/BN/defects.BN_331.N_B',
                'tree/BN/defects.BN_331.B_N']
    assert len(pathlist) == len(complist)

    for item in complist:
        assert item in pathlist


@pytest.mark.parametrize('vac', [True, False])
def test_apply_vacuum(vac):
    atoms = std_test_materials[1]
    host_dict = {'BN': atoms}
    def_dict = setup_defect_dict(host_dict=host_dict,
                                 general_algorithm=15.,
                                 uniform_vacuum=vac)
    def_dict = def_dict['BN']
    assert len(def_dict) > 0
    for def_key, structure in def_dict.items():
        cell = structure.get_cell()
        ref = (cell.lengths()[0] + cell.lengths()[1]) / 2.
        if vac:
            assert cell[2, 2] == pytest.approx(ref)
        else:
            assert cell[2, 2] == pytest.approx(
                atoms.get_cell().lengths()[2])


def test_setup_supercell(testdir):
    from defects.tasks.setup_defects import setup_supercell

    atoms = [std_test_materials[1], GaAs]
    dim = [True, False]
    x = [6, 4]
    y = [6, 4]
    z = [1, 4]
    for i, atom in enumerate(atoms):
        structure, N_x, N_y, N_z = setup_supercell(atom,
                                                   15,
                                                   dim[i])
        assert N_x == x[i]
        assert N_y == y[i]
        assert N_z == z[i]
        assert len(structure) == x[i] * y[i] * z[i] * len(atom)


def test_intrinsic_single_defects(testdir):
    lengths = [1, 4, 1]  # Number of defects that should be created
    materials = std_test_materials_dict.copy()
    materials.pop('Agchain')
    def_dict = setup_defect_dict(materials)
    assert len(def_dict) == len(materials)
    for i, host in enumerate(materials):
        # Check so that pristine material is in dict
        pristine = [key for key in def_dict[host] if 'pristine' in key.lower()]
        assert len(pristine) == 1
        # number of defects + pristine
        assert len(def_dict[host]) == lengths[i] + 1


def test_chemical_elements(testdir):
    from defects.tasks.setup_defects import add_intrinsic_elements
    results = {'Si2': ['Si'],
               'BN': ['B', 'N'],
               'Ag': ['Ag'],
               'Fe': ['Fe']}
    for i, atoms in enumerate(std_test_materials):
        name = atoms.get_chemical_formula()
        elements = add_intrinsic_elements(atoms, elements=[])
        for element in elements:
            assert element in results[name]
            assert len(elements) == len(results[name])


def test_extrinsic_single_defects(repo):
    lengths = [3, 8, 3]  # Number of defects that should be created
    materials = std_test_materials_dict.copy()
    materials.pop('Agchain')
    def_dict = setup_defect_dict(materials, extrinsic='V,Nb')
    assert len(def_dict) == len(materials)
    for i, host in enumerate(materials):
        # Check so that pristine material is in dict
        pristine = [key for key in def_dict[host] if 'pristine' in key.lower()]
        assert len(pristine) == 1
        # number of defects + pristine
        assert len(def_dict[host]) == lengths[i] + 1


@pytest.mark.parametrize('double_type', ['vac-vac',
                                         'vac-sub',
                                         'sub-sub'])
def test_extrinsic_double_defects(double_type, repo):
    lengths = {'vac-vac': 8,
               'vac-sub': 12,
               'sub-sub': 13}
    host = 'BN'
    materials = {host: std_test_materials_dict[host]}
    def_dict = setup_defect_dict(materials,
                                 extrinsic='Nb',
                                 double=double_type,
                                 scaling_double=1.5)
    assert len(def_dict) == len(materials)
    # Check so that pristine material is in dict
    pristine = [key for key in def_dict[host] if 'pristine' in key.lower()]
    assert len(pristine) == 1
    # number of defects + pristine
    assert len(def_dict[host]) == lengths[double_type] + 1


@pytest.mark.parametrize('double_type', ['vac-vac',
                                         'vac-sub',
                                         'sub-sub'])
def test_exclude_double_defects(double_type, repo):
    lengths = {'vac-vac': 10,
               'vac-sub': 17,
               'sub-sub': 21}
    host = 'BN'
    materials = {host: std_test_materials_dict[host]}
    def_dict = setup_defect_dict(materials,
                                 extrinsic='Nb,Yb',
                                 double=double_type,
                                 double_exclude='Yb',
                                 scaling_double=1.5)
    assert len(def_dict) == len(materials)

    # Check so that pristine material is in dict
    pristine = [key for key in def_dict[host] if 'pristine' in key.lower()]
    assert len(pristine) == 1
    # number of defects + pristine
    assert len(def_dict[host]) == lengths[double_type] + 1


@pytest.mark.parametrize('M', ['Mo', 'W'])
@pytest.mark.parametrize('X', ['S', 'Se', 'Te'])
@pytest.mark.parametrize('scaling', [0, 1, 1.5, 2])
def test_get_maximum_distance(M, X, scaling):
    from defects.tasks.setup_defects import get_maximum_distance
    from ase.data import atomic_numbers, covalent_radii
    from ase.build import mx2

    atoms = mx2(f'{M}{X}2')
    metal = atomic_numbers[M]
    chalcogen = atomic_numbers[X]
    reference = ((covalent_radii[metal] + covalent_radii[chalcogen])
                 * scaling)

    R = get_maximum_distance(atoms, 0, 1, scaling)
    assert R == pytest.approx(reference)


def test_new_double():
    from defects.tasks.setup_defects import is_new_double_defect

    complex_list = ['v_N.v_B', 'Cr_N.N_B', 'N_B.B_N', 'Nb_B.F_N']
    newlist = ['N_B.Cr_N', 'Cr_N.N_B', 'v_N.v_B', 'F_N.I_B', 'V_N.v_B']
    refs = [False, False, False, True, True]
    for i, new in enumerate(newlist):
        el1 = new.split('.')[0]
        el2 = new.split('.')[1]
        assert is_new_double_defect(el1, el2, complex_list) == refs[i]


# XXX Should be part of sj_analyse wf
@pytest.mark.xfail
def test_setup_halfinteger(asr_tmpdir):
    from pathlib import Path
    from asr.core import chdir
    from asr.setup.defects import main
    from ase.io import write
    from .materials import std_test_materials

    atoms = std_test_materials[1]
    write('unrelaxed.json', atoms)
    main()
    p = Path('.')
    pathlist = list(p.glob('defects.*/charge_0'))
    for path in pathlist:
        with chdir(path):
            write('structure.json', atoms)
            main(halfinteger=True)
            plus = Path('sj_+0.5/params.json')
            minus = Path('sj_-0.5/params.json')
            assert plus.is_file()
            assert minus.is_file()


# XXX Should be part of sj_analyse wf
@pytest.mark.xfail
def test_write_halfinteger(asr_tmpdir):
    from pathlib import Path
    from asr.core import chdir, read_json
    from asr.setup.defects import main, write_halfinteger_files
    from ase.io import write
    from .materials import std_test_materials

    materials = std_test_materials.copy()
    materials.pop(2)
    for atoms in materials:
        Path(f'{atoms.get_chemical_formula()}').mkdir()
        write(f'{atoms.get_chemical_formula()}/unrelaxed.json', atoms)
        with chdir(f'{atoms.get_chemical_formula()}'):
            main()
            p = Path('.')
            pathlist = list(p.glob('defects.*/charge_*'))
            for path in pathlist:
                with chdir(path):
                    write('structure.json', atoms)
                    params = read_json('params.json')
                    charge = int(str(
                        path.absolute()).split('/')[-1].split('_')[-1])
                    write_halfinteger_files(0.5, '+0.5', params, charge, '.')
                    write_halfinteger_files(-0.5, '-0.5', params, charge, '.')
                    params_p = read_json('sj_+0.5/params.json')
                    params_m = read_json('sj_-0.5/params.json')
                    deltas = [0.5, -0.5]
                    for i, par in enumerate([params_p, params_m]):
                        assert (par['asr.gs@calculate']['calculator']['charge']
                                == charge + deltas[i])
