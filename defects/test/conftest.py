import os
from pathlib import Path
import pytest


@pytest.fixture
def testdir(tmp_path):
    cwd = Path.cwd()
    try:
        os.chdir(tmp_path)
        yield tmp_path
    finally:
        os.chdir(cwd)
        print('tmp_path:', tmp_path)


@pytest.fixture
def repo(testdir):
    from taskblaster.repository import Repository
    with Repository.create(testdir, modulename='defects') as repo:
        yield repo
