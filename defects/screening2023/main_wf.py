import taskblaster as tb
from defects.screening2023.masterworkflow import ScreeningWorkflow

# Main workflow function


@tb.parametrize_glob('*/material')
def workflow(material):

    # XXX Need to double check calculators against ASR defaults
    calc_relax = {'mode': {
                      'name': 'pw',
                      'ecut': 800,
                      'dedecut': 'estimate'},
                  'xc': 'PBE',
                  'mixer':{'method':'fullspin','backend':'pulay'},
                  'kpts': {
                      'density': 3.0,
                      'gamma': True},
                  'basis': 'dzp',
                  'symmetry': {
                      'symmorphic': False},
                  'convergence': {
                      'forces': 1e-4,
                      'maximum iterations': 200},
                  'txt': 'relax.txt',
                  'occupations': {
                      'name': 'fermi-dirac',
                      'width': 0.02},  # Note: reduced fermi smearing again
                  'spinpol': True,
                  'maxiter': 400}

    # Low params calculator with only Gamma point and smaller ecut
    calc_lowparams = {'mode': {
                          'name': 'pw',
                          'ecut': 500,
                          'dedecut': 'estimate'},
                      'xc': 'PBE',
                      'mixer':{'method':'fullspin','backend':'pulay'},
                      'kpts': {
                          'size': [1, 1, 1],
                          'gamma': True},
                      'basis': 'dzp',
                      'symmetry': {
                          'symmorphic': False},
                      'convergence': {
                          'forces': 1e-3,
                          'maximum iterations': 200},
                      'txt': 'relax.txt',
                      'occupations': {
                          'name': 'fermi-dirac',
                          'width': 0.02},
                      'spinpol': True,
                      'maxiter': 400}

    calc_gs = {'mode': {
                   'name': 'pw',
                   'ecut': 800,
                   'dedecut': 'estimate'},
               'xc': 'PBE',
               'kpts': {
                   'density': 3.0,
                   'gamma': True},
               'basis': 'dzp',
               'symmetry': {
                   'symmorphic': False},
               'convergence': {
                   'bands': "CBM+3.0"},
               'nbands': "200%",
               'txt': 'gs.txt',
               'occupations': {
                   'name': 'fermi-dirac',
                   'width': 0.02},
               'spinpol': True,
               'maxiter': 400}

    oqmd = '/home/niflheim/fafb/db/oqmd12.db'
    c2db = '/home/niflheim2/cmr/WIP/doubledefects/databases/c2db.db'
    host_structure_folder = '/home/niflheim2/cmr/WIP/defect-screening'\
                            '-2023/calculations/Fredrik/hosts/'
    max_eform = 10.

    return ScreeningWorkflow(initial_atoms=material,
                             calc_lowparams=calc_lowparams,
                             calc_relax=calc_relax,
                             calc_gs=calc_gs,
                             oqmd=oqmd,
                             c2db=c2db,
                             host_folder=host_structure_folder,
                             max_eform=max_eform)
