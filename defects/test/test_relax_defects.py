from defects.test.materials import std_test_materials_dict
import numpy as np


def test_relax_fixcell(repo):
    """
    slightly stupid test only meant to check that the calculations
    do not crash. Actual structure optimization is tested in gpaw and in
    asrlib
    """
    from defects.tasks.relax import relax_fixcell

    initial_atoms = std_test_materials_dict['BN']
    calculator = {'mode': {'name': 'pw', 'ecut': 200}, 'xc': 'PBE',
                  'kpts': {'density': 1},
                  'convergence': {'forces': 5e-1}, 'smax': 5e-2,
                  'txt': 'relax.txt'
                  }

    output = relax_fixcell(initial_atoms, calculator)
    relaxed_atoms = output['atoms']

    assert np.allclose(relaxed_atoms.positions, initial_atoms.positions)
    assert np.allclose(relaxed_atoms.cell, initial_atoms.cell)
