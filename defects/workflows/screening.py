import taskblaster as tb


@tb.workflow
class LowParamsScreening:
    """
    Workflow for performing structural relaxation with low
    params and screen for defects with small formation
    energies
    """
    initial_atoms = tb.var()
    calc = tb.var()
    oqmd = tb.var()
    c2db = tb.var()
    host_folder = tb.var()
    max_eform = tb.var()

    @tb.task
    def relax_lowparams(self):
        return tb.node('defects.tasks.relax.relax_fixcell',
                       atoms=self.initial_atoms,
                       calculator=self.calc,
                       charge=0)

    @tb.task
    def collect_results(self):
        return tb.node('defects.tasks.eform.get_energies_and_structures',
                       atoms=self.relax_lowparams['atoms'],
                       subdir='LowParams/relax_lowparams',

                       filename='structure.json')

    @tb.task
    def formation_energies(self):
        return tb.node('defects.tasks.eform.get_all_eforms',
                       atoms=self.relax_lowparams['atoms'],
                       oqmd=self.oqmd,
                       c2db=self.c2db,
                       host_path=self.host_folder,
                       etot_def=self.collect_results['etot_def'],
                       etot_pris=self.collect_results['etot_pris'])

    @tb.task
    def physical_defect(self):
        """
        Estimation of formation energy and discards defect
        with formation energy >  max_eform
        """
        return tb.node('defects.tasks.eform.physical_defect',
                       eforms_dict=self.formation_energies,
                       max_eform=self.max_eform)
