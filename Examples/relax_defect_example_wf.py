import taskblaster as tb
from defects.workflows.relax_defects import RelaxDefectWorkflow
 
# Relaxation workflow function
@tb.parametrize_glob('*/material')
def workflow(material):

    calculator = {'mode': {
                      'name': 'pw',
                      'ecut': 800,
                      'dedecut': 'estimate'},
                  'xc': 'PBE',
                  'kpts': {
                      'density': 6.0,
                      'gamma': True},
                  'basis': 'dzp',
                  'symmetry': {
                      'symmorphic': False},
                  'convergence': {
                      'forces': 1e-4},
                  'txt': 'relax.txt',
                  'occupations': {
                      'name': 'fermi-dirac',
                      'width': 0.02},
                  'spinpol': True}

    return RelaxDefectWorkflow(initial_atoms=material, calc=calculator)
