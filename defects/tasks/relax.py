from ase import Atoms
from pathlib import Path
from ase.io import write
import numpy as np
from ase.calculators.calculator import PropertyNotImplementedError


def relax_fixcell(atoms: Atoms, calculator:  dict,
                  comm=None, charge=0, **kwargs) -> dict:
    """
    Perform a structure relaxation with fixed cell using GPAW.
    Specifying smax and fmax in the calculator
    dictionary will set stress and force tolerances, respectively.

    :param atoms: Atoms object to relax.
    :param calculator: A dictionary containing the calculator input parameters.
        If the stress 'smax' and force 'fmax' tolerances are not provided a
        value of 0.005 and 0.05 is assumed.
    :param comm: parallel file communicator.
    :param kwargs: Other keyword args for run_optimization.
    :return: dict of {'atoms': Atoms object (relaxed), 'traj_path': Path
    object}
    """
    from asrlib.tasks.factory import GPAWFactory
    from asrlib.tasks.relax import run_optimization
    from asrlib.tasks.mpiprocesses import gpaw
    from ase.io.trajectory import Trajectory

    if comm is None:
        from ase.parallel import world
        comm = world

    # try to restart from last trajectory, if present
    try:
        traj = Trajectory('relax.traj')
        atoms = traj[-1]
        parprint('Reading atoms from trajectory file ...', comm)
    except IOError:
        parprint('No existing trajectory file, start from scratch.', comm)
    except IndexError:
        parprint('Trajectory file empty, start from initial structure')

    # Make sure all ranks have loaded the same atoms file
    comm.barrier()

    calculator['charge'] = charge
    factory = GPAWFactory(calculator=calculator)
    factory.setup_kpts(atoms)

    # set initial magnetic moment
    if not atoms.has('initial_magmoms') or calculator.get('spinpol'):
        atoms.set_initial_magnetic_moments(np.ones(len(atoms), float))

    # defect calc is always with fixed cell
    smask = np.array([0, 0, 0, 0, 0, 0])

    # Construct & run default optimizer
    with gpaw(factory, comm) as atoms.calc:
        atoms, traj = run_optimization(atoms, fmax=factory.fmax,
                                       smax=factory.smax, comm=comm,
                                       identifier='relax',
                                       smask=smask,
                                       **kwargs)

    # Check magnetic moment
    try:
        magmoms = atoms.get_magnetic_moments()
    except PropertyNotImplementedError:
        # We assume this means that the magnetic moments are zero
        # for this calculator.
        magmoms = np.zeros(len(atoms))

    # If magnetic moment is small relax again with zero magnetic moment
    if not abs(magmoms).max() > 0.1:
        atoms.set_initial_magnetic_moments([0] * len(atoms))

        # re-initialize calculator and update txt log file name
        nm_factory = factory.copy()
        nm_factory.update({'txt': "relax-nm.txt"})

        with gpaw(factory, comm) as atoms.calc:
            atoms, traj = run_optimization(atoms, fmax=nm_factory.fmax,
                                           smax=nm_factory.smax, comm=comm,
                                           identifier='relax-nm',
                                           smask=smask,
                                           **kwargs)

    write('structure.json', atoms)
    matpath = Path('.')
    return {'atoms': atoms.copy(), 'traj_path': traj, 'path': matpath}


def parprint(message, comm):
    if comm.rank == 0:
        print(message)
