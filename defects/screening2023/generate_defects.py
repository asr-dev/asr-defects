from defects.workflows.totree import defecttotree  # , add_materials_from_db
from defects.tasks.setup_defects import setup_defect_dict
from ase.io import read


# Host: XXX EDIT FOR YOUR PURPOSE
host = 'BN'

kwargs = {'general_algorithm': 15.,
          'double': 'all',  # all kinds of double defects
          'scaling_double': 1.5}        # are in list


# read structure downloaded from c2db
hostfile = f'hosts/structure_{host}.json'
print('Host', host)
print(f'Reading host atoms from {hostfile}')

atoms = read(hostfile)
host_dict = {host: atoms}

defects = {}

metals = ['B', 'C', 'N', 'F', 'Al', 'Si', 'P', 'S', 'Cl', 'Sc', 'Ti',
          'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As',
          'Se', 'Br']

# Removing host atoms from extrinsic substitutions
for hostatom in atoms.get_chemical_symbols():
    if hostatom in metals:
        metals.remove(hostatom)

# Slightly cumbersome and slow but needed since we have to generate
# only X-X and O-X extrinsic double defects
num_metals = len(metals)
for i, metal in enumerate(metals):
    print(f'setting up for extrinsic element {i+1}/{num_metals}: {metal}')
    kwargs['extrinsic'] = f'O,{metal}'
    print("kwargs:")
    print(kwargs)
    defects_tmp = setup_defect_dict(host_dict, **kwargs)[host]
    defects.update(defects_tmp)

# We only want to do calculations for one host at a time
defect_dict = {host: defects}

# totree workflow
workflow = defecttotree(defect_dict,
                        name='material')
