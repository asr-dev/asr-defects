"""
Dedicated worker module will not be necessary in newer taskblaster.
Therefore, this module can be renamed.
"""


class TBCommunicator:
    """GPAW/taskblaster communicator compatibility layer."""

    def __init__(self, comm):
        self._comm = comm  # GPAW communicator
        self.rank = comm.rank
        self.size = comm.size

    def broadcast_object(self, obj):
        from ase.parallel import broadcast
        return broadcast(obj, root=0, comm=self._comm)

    def split(self, newsize):
        import numpy as np
        assert self.size % newsize == 0

        mygroup = self.rank // newsize

        startrank = mygroup * newsize
        endrank = startrank + newsize
        ranks = np.arange(startrank, endrank)
        _comm = self._comm.new_communicator(ranks)
        assert _comm is not None
        return TBCommunicator(_comm)
